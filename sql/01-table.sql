-- users
CREATE TABLE IF NOT EXISTS usr (
	id INT AUTO_INCREMENT,
	hash VARCHAR(20) NOT NULL,
	salt VARCHAR(25) NOT NULL,
	email VARCHAR(90) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (email)
);
CREATE TABLE IF NOT EXISTS producer (
	id INT,
	PRIMARY KEY (id),
	FOREIGN KEY (id) REFERENCES usr (id)
);
CREATE TABLE IF NOT EXISTS admn (
	id INT,
	PRIMARY KEY (id),
	FOREIGN KEY (id) REFERENCES usr (id)
);
-- lessons and their components
CREATE TABLE IF NOT EXISTS lesson (
	id INT AUTO_INCREMENT,
	producer INT NOT NULL,
	admn INT,
	name NVARCHAR(25) NOT NULL,
	FOREIGN KEY (producer) REFERENCES producer (id),
	FOREIGN KEY (admn) REFERENCES admn (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS quiz (
	id INT AUTO_INCREMENT,
	lesson INT NOT NULL,
	FOREIGN KEY (lesson) REFERENCES lesson (id),
	name NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS activity (
	id INT AUTO_INCREMENT,
	lesson INT NOT NULL,
	FOREIGN KEY (lesson) REFERENCES lesson (id),
	name NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS article (
	id INT AUTO_INCREMENT,
	lesson INT NOT NULL,
	FOREIGN KEY (lesson) REFERENCES lesson (id),
	name NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS subject (
	id INT AUTO_INCREMENT,
	name NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS subject_set (
	lesson INT,
	subject INT,
	FOREIGN KEY (lesson) REFERENCES lesson (id),
	FOREIGN KEY (subject) REFERENCES subject (id),
	CONSTRAINT pk PRIMARY KEY (lesson, subject)
);
-- user relationships with lessons and quizes
CREATE TABLE IF NOT EXISTS score (
	usr INT,
	quiz INT,
	val INT NOT NULL,
	FOREIGN KEY (usr) REFERENCES usr (id),
	FOREIGN KEY (quiz) REFERENCES quiz (id),
	CONSTRAINT pk PRIMARY KEY (usr, quiz)
);
-- article components
CREATE TABLE IF NOT EXISTS section (
	id INT AUTO_INCREMENT,
	article INT NOT NULL,
	name NVARCHAR(25) NOT NULL,
	FOREIGN KEY (article) REFERENCES article (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS element (
	id INT AUTO_INCREMENT,
	section INT NOT NULL,
	FOREIGN KEY (section) REFERENCES section (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS text (
	id INT,
	data NVARCHAR(255) NOT NULL,
	FOREIGN KEY (id) REFERENCES element (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS picture (
	id INT,
	data BLOB NOT NULL,
	FOREIGN KEY (id) REFERENCES element (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS video (
	id INT,
	data BLOB NOT NULL,
	FOREIGN KEY (id) REFERENCES element (id),
	PRIMARY KEY (id)
);
-- quiz components
CREATE TABLE IF NOT EXISTS question (
	id INT AUTO_INCREMENT,
	quiz INT NOT NULL,
	data NVARCHAR(255) NOT NULL,
	FOREIGN KEY (quiz) REFERENCES quiz (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS fill_blank (
	id INT,
	regex VARCHAR(255) NOT NULL,
	FOREIGN KEY (id) REFERENCES question (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS multi_choice (
	id INT,
	FOREIGN KEY (id) REFERENCES question (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS choice (
	id INT AUTO_INCREMENT,
	data NVARCHAR(25) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS common_choice (
	id INT,
	FOREIGN KEY (id) REFERENCES choice (id),
	PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS choice_set (
	choice INT,
	question INT,
	FOREIGN KEY (question) REFERENCES multi_choice (id),
	FOREIGN KEY (choice) REFERENCES choice (id),
	CONSTRAINT pk PRIMARY KEY (choice, question)
);
CREATE TABLE IF NOT EXISTS correct_set (
	choice INT,
	question INT,
	set_id INT NOT NULL,
	FOREIGN KEY (choice) REFERENCES choice (id),
	FOREIGN KEY (question) REFERENCES multi_choice (id),
	CONSTRAINT pk PRIMARY KEY (choice, question, set_id)
);
